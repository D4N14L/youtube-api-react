import axios from "axios";

export const API_KEY = "AIzaSyAlDxHVDlJAU9na04S4TqRTP2bPZPfOr-c";

export const YoutubeRequest = axios.create({
  baseURL: "https://www.googleapis.com/youtube/v3",
  params: {
    part: "snippet",
    maxResults: 25,
    key: API_KEY
  }
});

import React, { Component } from "react";
import SearchBar from "./SearchBar";
import { YoutubeRequest } from "../apis/Youtube";
import VideoList from "./VideoList";
import VideoDetail from "./VideoDetail";
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      videos: [],
      selectedVideo: null
    };
    this.videoDetail = React.createRef();
  }
  componentDidMount() {
    this.SearchHandler("music");
  }
  SearchHandler = async term => {
    const videos = await YoutubeRequest.get("/search", {
      params: {
        q: term
      }
    });
    this.setState({
      videos: videos.data.items,
      selectedVideo: videos.data.items[0]
    });
  };

  onVideoSelect = video => {
    this.setState({ selectedVideo: video });
    window.scroll(0, 0);
  };

  render() {
    return (
      <div className="ui container">
        <SearchBar SearchHandler={this.SearchHandler} />
        <div className="ui grid">
          <div className="row">
            <div className="eleven wide column">
              <VideoDetail
                ref={this.videoDetail}
                video={this.state.selectedVideo}
              />
            </div>
            <div className="five wide column">
              <VideoList
                VideoSelect={this.onVideoSelect}
                videos={this.state.videos}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

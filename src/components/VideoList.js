import React from "react";
import VideoItem from "./VideoItem";
export default function VideoList({ videos, VideoSelect }) {
  const renderedList = videos.map((video, index) => {
    return <VideoItem key={index} VideoSelect={VideoSelect} video={video} />;
  });

  return <div className="ui relaxed divided list">{renderedList}</div>;
}

import React from "react";

export default function VideoDetail({ video }) {
  if (!video) {
    return "Loading...";
  }
  const videoId = `https://www.youtube.com/embed/${video.id.videoId}`;
  return (
    <div>
      <div className="ui segment">
        <div className="ui embed">
          <iframe title={video.snippet.title} src={videoId} />
        </div>
        <h4 className="ui header">{video.snippet.title}</h4>
        <p>{video.snippet.description}</p>
      </div>
    </div>
  );
}

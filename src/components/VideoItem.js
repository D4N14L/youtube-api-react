import "./VideoItem.css";
import React from "react";

export default function VideoItem({ video, VideoSelect }) {
  return (
    <div onClick={event => VideoSelect(video)} className="item video-item">
      <img
        className="ui image"
        src={video.snippet.thumbnails.medium.url}
        alt={video.snippet.title}
      />
      <div className="content">
        <div className="header">{video.snippet.title}</div>
      </div>
    </div>
  );
}

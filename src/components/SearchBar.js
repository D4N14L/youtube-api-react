import React, { Component } from "react";

export default class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      term: ""
    };
  }
  render() {
    return (
      <div className="ui segment search-bar">
        <form
          onSubmit={event => {
            event.preventDefault();
            this.props.SearchHandler(this.state.term);
          }}
          className="ui form"
        >
          <div className="field">
            <input
              value={this.state.term}
              onChange={event => this.setState({ term: event.target.value })}
              type="text"
              placeholder="Search Video"
            />
          </div>
        </form>
      </div>
    );
  }
}
